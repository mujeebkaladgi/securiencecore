package com.securience.core.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.securience.core.errorhandling.SecErrorCode;
import com.securience.core.errorhandling.SecException;
import com.securience.core.logging.SecLogger;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class OkHttpSecNetworkImpl implements SecNetworkClient {

	@Override
	public SecHttpResponse invoke(SecHttpRequest secHttpRequest) throws SecException{


		MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
		RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials");
		Request request = new Request.Builder()
		  .url("http://localhost:8081/identityiq/oauth2/token")
		  .method("POST", body)
		  .addHeader("Authorization", "Basic TjZRSzRPQ3o1dG1qQk5VRHQ0VFI0TWRjeENhajJaTFY6bklwN091ZFZUQkV2U3IzYw==")
		  .addHeader("Content-Type", "application/x-www-form-urlencoded")
		  .addHeader("Cookie", "JSESSIONID=ACA747FFBDC284E02DEF57E48CCD0E71")
		  .build();
		
		
		SecLogger.debug("Invoking http API : "+secHttpRequest.getURL().toString() );
		HttpClient client = HttpClientBuilder.create().build();
	
		NetworkUtil.populateContentTypeHeaders(secHttpRequest);
		UrlEncodedFormEntity formEntity = null;
		//if (secHttpRequest.getPayloadType().equals(SecHttpRequest.PayloadType.URLENCODED)) {
		//	formEntity = NetworkUtil.createFormEntity(secHttpRequest.getFormParams());
		//}
		String url = secHttpRequest.getURL().toString();
	
		return null;
		
	}

	

}
