package com.securience.core.network;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An Http Api Response. Instances of this class are immutable.
 */
public interface SecHttpResponse {


    enum PayloadType {JSON, STRING}



    /**
     * @return Request method of this request. Please refer to
     * {@link PayloadType} for supporting method
     */
    PayloadType getPayloadType();

    /**
     * @return A list of HTTP headers for this request
     */
    Map<String, String> getHeaders();


    /**
     * @return The body to be sent with this request
     */
    Object getBody();



    /**
     * Object builder class for {@link SecHttpResponse}
     */
    class SecResponseBuilder {

  
        private Map<String, String> headers = new HashMap<>();
        private Object body;

        private PayloadType payloadType = PayloadType.JSON;

        /**
         * Create a build with provided {@link URL}
         *
         * @param url the provided URL
         */

        public SecResponseBuilder (Object body) {
        	this.body = body;
        }

        /**
         * Convenience method for reconstructing a response as a builder
         * @param response SecResponse object containing response details
         */
        public SecResponseBuilder(SecHttpResponse response) {
            
            this.body = response.getBody();
            this.payloadType = response.getPayloadType();
            this.headers = response.getHeaders();
       
        }


        /**
         * Set the response payload type
         * @param type response payload type
         */
        public SecResponseBuilder payloadType(PayloadType type) {
            this.payloadType = type;

            return this;
        }

        



        /**
         * Sets a list request headers
         * @param headers A map of header name and header value
         * @return The builder
         */
        public SecResponseBuilder setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        /**
         * Adds the specified header to the request.
         *
         * @param name  Header name
         * @param value Header value
         * @return The builder
         */
        public SecResponseBuilder addHeader(String name, String value) {
            headers.put(name, value);
            return this;
        }

        /**
         * Removes the specified header from the request.
         *
         * @param name Header name
         * @return The builder
         */
        public SecResponseBuilder removeHeader(String name) {
            headers.remove(name);
            return this;
        }


        /**
         * Builds the {@link SecHttpResponse} object.
         *
         * @return An immutable {@link SecHttpResponse} object.
         */
        public SecHttpResponse build() {



            return new SecHttpResponse() {

                

                @Override
                public PayloadType getPayloadType() {


                    return payloadType;
                }

                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }

             

                @Override
                public Object getBody() {
                    return body;
                }


            };
        }
    }

}
