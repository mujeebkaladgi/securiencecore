package com.securience.core.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.securience.core.errorhandling.SecErrorCode;
import com.securience.core.errorhandling.SecException;
import com.securience.core.logging.SecLogger;

public class ApacheHttpSecNetworkImpl implements SecNetworkClient {

	@Override
	public SecHttpResponse invoke(SecHttpRequest secHttpRequest) throws SecException{

		SecLogger.debug("Invoking http API : "+secHttpRequest.getURL().toString() );
		HttpClient client = HttpClientBuilder.create().build();
	
		NetworkUtil.populateContentTypeHeaders(secHttpRequest);
		UrlEncodedFormEntity formEntity = null;
		//if (secHttpRequest.getPayloadType().equals(SecHttpRequest.PayloadType.URLENCODED)) {
		//	formEntity = NetworkUtil.createFormEntity(secHttpRequest.getFormParams());
		//}
		String url = secHttpRequest.getURL().toString();
	
		
		HttpResponse response = null;
		try {
			if (secHttpRequest.getMethod().equalsIgnoreCase(SecHttpRequest.Method.GET.name())) {
				SecLogger.debug("HTTP Method : GET");
				HttpGet request = new HttpGet(url);
				secHttpRequest.getHeaders().forEach((key, value) -> {
					request.addHeader(key, value);
					SecLogger.debug("Adding header " + key +" : " + value);
				});

				
				response = client.execute(request);
			} else if (secHttpRequest.getMethod().equalsIgnoreCase(SecHttpRequest.Method.POST.name())) {
				SecLogger.debug("HTTP Method : POST");
				HttpPost request = new HttpPost(url);
				secHttpRequest.getHeaders().forEach((key, value) -> {
					request.addHeader(key, value);
					SecLogger.debug("Adding header " + key +" : " + value);
				});
				//if (formEntity != null) {
					request.setEntity(formEntity );
				//} else {
					HttpEntity entity = new StringEntity((String)secHttpRequest.getBody());
					request.setEntity(entity );
				//}
				
				
				response = client.execute(request);
			} else if (secHttpRequest.getMethod().equalsIgnoreCase(SecHttpRequest.Method.PUT.name())) {
				SecLogger.debug("HTTP Method : PUT");
				HttpPut request = new HttpPut(url);
				secHttpRequest.getHeaders().forEach((key, value) -> {
					request.addHeader(key, value);
					SecLogger.debug("Adding header " + key +" : " + value);
				});

				SecLogger.debug("Request body " + (String)secHttpRequest.getBody());
				HttpEntity entity = new StringEntity((String)secHttpRequest.getBody());
				request.setEntity(entity );
				response = client.execute(request);

			}
		} catch (ClientProtocolException e) {
			SecLogger.error("Error while making API call :" + e.getMessage());
			throw new SecException(SecErrorCode.CLIENT_PROTOCOL_EXCEPTION, e);
		} catch (IOException e) {
			SecLogger.error("Error while making API call :" + e.getMessage());
			throw new SecException(SecErrorCode.IO_EXCEPTION, e);
		}

		SecLogger.debug("Received API response " );
		// Get the response
		BufferedReader rd = null;
		try {
			rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		} catch (UnsupportedOperationException  e) {
			SecLogger.error("Error while reading API response :" + e.getMessage());
			throw new SecException(SecErrorCode.UNSUPPORTED_OPERTATION_EXCEPTION, e);
		} catch (IOException e) {
			SecLogger.error("Error while reading API response :" + e.getMessage());
			throw new SecException(SecErrorCode.IO_EXCEPTION, e);
		}

		String line = "";
		StringBuffer responseString = new StringBuffer("");
		try {
			while ((line = rd.readLine()) != null) {
				responseString.append(line);
			}
		} catch (IOException e) {
			SecLogger.error("Error while reading API response :" + e.getMessage());
			throw new SecException(SecErrorCode.IO_EXCEPTION, e);
		}

		SecLogger.debug("API response body : "  +responseString);

		Header[] responseHeaders = response.getAllHeaders();
		Map<String, String> secHeaders = new HashMap<String, String>();

		for (int i = 0; i < responseHeaders.length; i++) {
			Header responseHeader = responseHeaders[i];
			secHeaders.put(responseHeader.getName(), responseHeader.getValue());
			SecLogger.debug("API response header  "  +responseHeader.getName() + " : "+ responseHeader.getValue());
	
		}

		SecHttpResponse secResponse = new SecHttpResponse.SecResponseBuilder(responseString.toString())
				.setHeaders(secHeaders).build();

		return secResponse;
	}

	

}
