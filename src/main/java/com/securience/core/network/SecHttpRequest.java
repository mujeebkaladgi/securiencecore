package com.securience.core.network;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An Http Api Request. Instances of this class are immutable.
 */
public interface SecHttpRequest {

    enum Method {GET, PUT, POST, DELETE, PATCH}
    enum PayloadType {JSON, STRING,URLENCODED}

    /**
     * @return URL of this request
     */
    URL getURL();

    /**
     * @return Request method of this request. Please refer to
     * {@link Method} for supporting method
     */
    String getMethod();

    /**
     * @return Request method of this request. Please refer to
     * {@link PayloadType} for supporting method
     */
    PayloadType getPayloadType();

    /**
     * @return A list of HTTP headers for this request
     */
    Map<String, String> getHeaders();

    /**
     * @return A list of HTTP headers for this request
     */
    Map<String, String> getQueryParams();

    /**
     * @return A list of HTTP headers for this request
     */
    Map<String, String> getFormParams();
    
    
    /**
     * @return The body to be sent with this request
     */
    Object getBody();



    /**
     * Object builder class for {@link SecHttpRequest}
     */
    class SecHttpRequestBuilder {

        private URL url;
        private String method = Method.GET.name();
        private Map<String, String> headers = new HashMap<>();
        private Object body;
        private Map<String, String> queryParams = new HashMap<>();
        private Map<String, String> formParams = new HashMap<>();
        private PayloadType payloadType = PayloadType.JSON;

        /**
         * Create a build with provided {@link URL}
         *
         * @param url the provided URL
         */

        public SecHttpRequestBuilder (URL url) {
            this.url = url;
            //return  this;
        }

        /**
         * Convenience method for reconstructing a request as a builder
         * @param request SecRequest object containing request details
         */
        public SecHttpRequestBuilder(SecHttpRequest request) {
            this.method = request.getMethod();
            this.body = request.getBody();
            this.payloadType = request.getPayloadType();
            this.headers = request.getHeaders();
            this.url = request.getURL();
            this.formParams = request.getFormParams();
        }


        /**
         * Set the request payload type
         * @param type Request payload type
         */
        public SecHttpRequestBuilder payloadType(PayloadType type) {
            this.payloadType = type;

            return this;
        }

        /**
         * Sets the request method to GET.
         *
         * @return The builder
         */
        public SecHttpRequestBuilder get() {
            this.method = Method.GET.name();
            this.body = null;
            return this;
        }

        /**
         * Sets the request method to POST.
         *
         * @return The builder
         */
        public SecHttpRequestBuilder post(Object body) {
            this.method = Method.POST.name();
            this.body = body;
            return this;
        }

        /**
         * Sets the request method to PUT.
         *
         * @return The builder
         */
        public SecHttpRequestBuilder put(Object body) {
            this.method = Method.PUT.name();
            this.body = body;
            return this;
        }

        /**
         * Sets the request method to PATCH.
         *
         * @return The builder
         */
        public SecHttpRequestBuilder patch(Object body) {
            this.method = Method.PATCH.name();
            this.body = body;
            return this;
        }


        /**
         * Sets the request method to DELETE.
         *
         * @return The builder
         */
        public SecHttpRequestBuilder delete(Object body) {
            this.method = Method.DELETE.name();
            this.body = body;
            return this;
        }




        /**
         * Sets a list request headers
         * @param headers A map of header name and header value
         * @return The builder
         */
        public SecHttpRequestBuilder setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        
        /**
         * Adds the specified header to the request.
         *
         * @param name  Header name
         * @param value Header value
         * @return The builder
         */
        public SecHttpRequestBuilder addFormData(String name, String value) {
            formParams.put(name, value);
            return this;
        }
        
        
        /**
         * Adds the specified header to the request.
         *
         * @param name  Header name
         * @param value Header value
         * @return The builder
         */
        public SecHttpRequestBuilder addHeader(String name, String value) {
            headers.put(name, value);
            return this;
        }

        /**
         * Removes the specified header from the request.
         *
         * @param name Header name
         * @return The builder
         */
        public SecHttpRequestBuilder removeHeader(String name) {
            headers.remove(name);
            return this;
        }


        /**
         * Builds the {@link SecHttpRequest} object.
         *
         * @return An immutable {@link SecHttpRequest} object.
         */
        public SecHttpRequest build() {



            return new SecHttpRequest() {

                @Override
                public URL getURL() {
                    return url;
                }

                @Override
                public String getMethod() {
                    return method;
                }

                @Override
                public PayloadType getPayloadType() {


                    return payloadType;
                }

                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }

                @Override
                public Map<String, String> getQueryParams() {
                    return queryParams;
                }

                @Override
                public Map<String, String> getFormParams() {
                    return queryParams;
                }

                
                @Override
                public Object getBody() {
                    return body;
                }


            };
        }
    }

}
