package com.securience.core.network;

import com.securience.core.errorhandling.SecException;

public interface SecNetworkClient {

	public SecHttpResponse invoke(SecHttpRequest secHttpRequest) throws SecException;
}


