package com.securience.core.jwt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.securience.core.errorhandling.SecErrorCode;
import com.securience.core.errorhandling.SecException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtil {

	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);
	private static String SECRET_KEY = "oaRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0SAZtWYKHfM15g7A3HD4cVREf9cUsprCRK93w";

	public static final String JWT_EXPIRY_TIME = "JWT_EXPIRY_TIME";

	public static String create(Map<String, String> payload, int expiryTime) throws SecException {
		logger.debug("Entering JWTController create method");

		try {
			Date newDate = addExpiryTimeToCurrentDate(expiryTime);

			payload.put(JWT_EXPIRY_TIME, newDate.toString());
			// Let's set the JWT Claims
			Date now = new Date();
			JwtBuilder builder = Jwts.builder().setId(UUID.randomUUID().toString()).setIssuedAt(now).setClaims(payload)
					.signWith(getKey());

			// Builds the JWT and serializes it to a compact, URL-safe string
			return builder.compact();

		} catch (Exception e) {
			String errorMessage = "Error occured while creating jwt. Error: " + e.getMessage();
			logger.error(errorMessage);
			throw new SecException(SecErrorCode.JWT_CREATION_EXCEPTION, e);

		} finally {
			logger.debug("Leaving JWTController create method");
		}

	}

	public static String updateExpiry(String jwt, int expiryTime) throws SecException {
		Date newDate = addExpiryTimeToCurrentDate(expiryTime);
		return addOrUpdateClaimFromToken(jwt, JWT_EXPIRY_TIME, newDate.toString());
		
	}
	
	private static Date addExpiryTimeToCurrentDate(int expiryTimeInSeconds) {
		Date oldDate = new Date();
		Calendar gcal = new GregorianCalendar();
		gcal.setTime(oldDate);
		gcal.add(Calendar.SECOND, expiryTimeInSeconds);
		Date newDate = gcal.getTime();
		return newDate;
	}

	private static Date stringToDate(String dateStr) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", new Locale("us"));

		// Thu Feb 25 16:29:50 IST 2021
		return sdf.parse(dateStr);
	}

	public static boolean verify(String verifyReq) throws SecException {
		logger.debug("Entering JWTController verify method");
		String errorMessage = "";

		try {
			Jwts.parserBuilder().setSigningKey(getKey()).build().parseClaimsJws(verifyReq);
			String expiryDate = (String) getClaimsFromToken(verifyReq, JWT_EXPIRY_TIME);
			Date date = stringToDate(expiryDate);
			if (date.before(new Date())) {
				logger.error("JWT expired");
				throw new SecException(SecErrorCode.JWT_EXPIRED_EXCEPTION, new Exception());
			}

			return true;
		} catch (Exception e) {

			errorMessage = "Error occured while verifying jwt. Error: " + e.getMessage();
			logger.error(errorMessage);

			throw new SecException(SecErrorCode.JWT_VERIFICATION_EXCEPTION, e);
		} finally {
			logger.debug("Leaving JWTController verify method");
		}

	}

	public static Object getClaimsFromToken(String jwt, String key) throws SecException {
		Claims claims = null;
		logger.debug("Entering getClaimsFromToken method");
		try {
			claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(jwt).getBody();
			logger.debug("JWT claims are :" + claims.get(key));
		} catch (Exception e) {
			logger.debug("Error getting claims from JWT : " + e.getMessage());
			throw new SecException(SecErrorCode.JWT_CLAIM_EXCEPTION, e);
		}
		return claims.get(key);
	}

	public static String addOrUpdateClaimFromToken(String jwt, String key, Object value) throws SecException {
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(jwt).getBody();
			
			claims.put(key, value);
			Date now = new Date();
			JwtBuilder builder = Jwts.builder().setId(UUID.randomUUID().toString()).setIssuedAt(now).setClaims(claims)
					.signWith(getKey());

			// Builds the JWT and serializes it to a compact, URL-safe string
			return builder.compact();
		} catch (Exception e) {
			logger.debug("Error getting claims from JWT : " + e.getMessage());
			throw new SecException(SecErrorCode.JWT_CLAIM_EXCEPTION, e);
		}
		
	}

	public static Key getKey() {
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		return signingKey;
	}

	public static void main(String[] args) throws ParseException {
		Map<String, String> jwtPayload = new HashMap<String, String>();
		jwtPayload.put("key1", "value1");
		jwtPayload.put("key2", "value2");
		jwtPayload.put("key3", "value3");
		
		String jwt;
		try {
			jwt = JwtUtil.create(jwtPayload, 1000);
			System.out.println(jwt);
			
			
			String newJwt = addOrUpdateClaimFromToken(jwt, "key1", "newvalue");
			String value = (String)JwtUtil.getClaimsFromToken(newJwt, "key1");
			System.out.println(value);
		} catch (SecException e) {
			
				assert(false);
			
			
		}	
		

	}
}
