package com.securience.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.securience.core.errorhandling.SecErrorCode;
import com.securience.core.errorhandling.SecException;
import com.securience.core.logging.SecLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class SecUtil.
 */
public class SecUtil {

	private static final Logger logger = LoggerFactory.getLogger(SecUtil.class);
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getSecurienceHome());

	}

	/**
	 * Gets the securience home.
	 *
	 * @return the securience home
	 */
	public static String getSecurienceHome(String defaultPath) {

		SecLogger.info("Securience_Home => " + System.getenv("SECURIENCE_HOME"));

		String secHome = System.getenv("SECURIENCE_HOME");
		if (secHome != null) {
			return secHome;
		}
		SecLogger.warn("Securience Home is not set. It is recommended to set the SECURIENCE_HOME environment variable");
		return defaultPath;

	}

	public static String getSecurienceHome() {
		return getSecurienceHome(null);
	}
	/**
	 * Read properties file
	 *
	 * @return properties
	 */
	public static Map <String, String> readPropertiesFile(String path) throws SecException{

		Properties prop = new Properties();
		 Map <String, String>  response = new HashMap <String, String> ();
		try {
			
			prop.load(new FileInputStream(path));
			for (final String name: prop.stringPropertyNames())
				response.put(name, prop.getProperty(name));

		} catch (IOException ex) {
			logger.error(ex.toString());
			throw new SecException(SecErrorCode.IO_EXCEPTION, ex);
			
		} catch (Exception e) {
			logger.error(e.toString());
			throw new SecException(SecErrorCode.UNKNOWN_EXCEPTION, e);
		}
		return response;

	}

}
