package com.securience.core.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.securience.core.errorhandling.SecException;


// TODO: Auto-generated Javadoc
/**
 * This class defines a custom Securience Logger.
 */
public final class SecLogger {
    
    /** The Constant TAG. */
    private static final String TAG = "SECURIENCE :";
    
    /** The Constant LOG_ENABLE. */
    private static final boolean LOG_ENABLE = true;
    
    /** The Constant DETAIL_ENABLE. */
    private static final boolean DETAIL_ENABLE = true;

    /** The Constant Log. */
    private static final Logger Log = LoggerFactory.getLogger(SecLogger.class);
    
    /**
     * Instantiates a new sec logger.
     */
    private SecLogger() {
    }

    /**
     * Builds the msg.
     *
     * @param msg the msg
     * @param e the e
     * @return the string
     */
    private static String buildMsg(String msg, Throwable e) {
        StringBuilder buffer = new StringBuilder();

        if (DETAIL_ENABLE) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            buffer.append("[ ");
            buffer.append(Thread.currentThread().getName());
            buffer.append(": ");
            buffer.append(stackTraceElement.getFileName());
            buffer.append(": ");
            buffer.append(stackTraceElement.getLineNumber());
            buffer.append(": ");
            buffer.append(stackTraceElement.getMethodName());
        }

        buffer.append("() ] _____ ");

        if (msg != null)
        	buffer.append(TAG).append(msg);
        if (e != null) {
            buffer.append("  Exception details ").append(e.getMessage());
            if (e instanceof SecException) {
                buffer
                        .append("[ ")
                        .append("Error Code : ")
                        .append(((SecException)e).getErrorCode())
                        .append(": ")
                        .append("Error Message : ")
                        .append(((SecException)e).getErrorMessage())
                        .append("] ");
            }
        }

        return buffer.toString();
    }

    /**
     * Message to be logged in trace mode.
     * @param msg the message which is to be logged.
     */
    public static void trace(String msg) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.VERBOSE)*/) {
            Log.trace( buildMsg(msg, null));
        }
    }

    /**
     * Message to be logged in Debug.
     * @param msg Message to be logged.
     */
    public static void debug(String msg) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.DEBUG)*/) {
            Log.debug( buildMsg(msg, null));
        }
    }

    /**
     * Message to be logged in Info.
     * @param msg Message to be logged.
     */
    public static void info(String msg) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.INFO)*/) {
            Log.info( buildMsg(msg, null));
        }
    }

    /**
     * Message to be logged in Warn.
     * @param msg Message to be logged.
     */
    public static void warn(String msg) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.WARN)*/) {
            Log.warn( buildMsg(msg, null));
        }
    }

    /**
     * Message to be logged in Warn if exception occurs.
     * @param msg Message to be logged.
     * @param e Exception
     */
    public static void warn(String msg, Exception e) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.WARN)*/) {
            Log.warn( buildMsg(msg, e), e);
        }
    }

    /**
     * Message to be logged in Error.
     * @param msg Message to be logged.
     */
    public static void error(String msg) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.ERROR)*/) {
            Log.error( buildMsg(msg, null));
        }
    }

    /**
     * Message to be logged in Error if exception occurs.
     * @param msg Message to be logged.
     * @param e Exception
     */
    public static void error(String msg, Exception e) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.ERROR)*/) {
            Log.error( buildMsg(msg, e), e);
        }
    }

    /**
     * Message to be logged stating the exception.
     * @param e exception
     */
    public static void error( Exception e) {
        if (LOG_ENABLE /*&& Log.isLoggable( Log.ERROR)*/) {
            Log.error( buildMsg("", e), e);
        }
    }
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
    	SecLogger.debug("aba");
    }
}