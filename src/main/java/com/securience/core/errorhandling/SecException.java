package com.securience.core.errorhandling;


// TODO: Auto-generated Javadoc
/**
 * Custom Securience Exception class.
 */
public class SecException extends Exception {

    /** The error code. */
    private String errorCode;
    
    /** The error message. */
    private String errorMessage;



    /**
     * Instantiates a new sec exception.
     *
     * @param e the e
     */
    public SecException(Throwable e) {
        super(e);
    }



    /**
     * Create SecException object.
     *
     * @param errorCode Error code
     * @param errorMessage Error message
     * @param e Generic exception
     */
    public SecException(String errorCode, String errorMessage, Exception e) {
        super(e);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }


    /**
     * Create SecException object.
     *
     * @param error the error
     * @param e the e
     */

    public SecException(SecErrorCode error, Throwable e) {
        super(e);
        this.errorCode = error.getErrorCode();
        this.errorMessage = error.getErrorMessage();
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */

    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Set the error code.
     * @param errorCode error code of the exception which occurs.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * Gets the error message.
     * @return the error message.
     */

    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Set the error message.
     * @param errorMessage error message of the exception which occurs.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    
}
