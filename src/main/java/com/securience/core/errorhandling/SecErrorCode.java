package com.securience.core.errorhandling;

import com.securience.core.logging.SecLogger;

import io.swagger.annotations.ApiModelProperty;


// TODO: Auto-generated Javadoc
/**
 * This class shows a list of error codes and error messages for different exceptions which can occur.
 */
public class SecErrorCode {

    /** The error code. */
	@ApiModelProperty(notes="It prints error code",example ="2000")
    private String errorCode;
    
    /** The error message. */
	@ApiModelProperty(notes="It prints error message",example ="Operation successfull")
    private String errorMessage;

    /**
     * returns an error code of the type String.
     *
     * @return errorCode the particular error code is returned when
     * the corresponding exception occurs.
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * sets the error code.
     *
     * @param errorCode the errorCode from getErrorCode() is passed here
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *returns a particular errorMessage corresponding to the
     * type of exception which has occured.
     * @return errorMessage returns the corresponding errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * sets the corresponding error message.
     *
     * @param errorMessage the errorMessage from getErrorMessage() is passed here
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * contains the list of exceptions with the error codes and error messages which can occur.
     *
     * @param errorCode takes in the corresponding error code
     * @param errorMessage takes in the corresponding error message
     */
    public SecErrorCode(String errorCode, String errorMessage) {
    	this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * The exception is thrown when biometric authentication fails.
     */
    public static final SecErrorCode SUCCESS = SecErrorCodeUtil.getSecErrorCode("1000", "Operation successfully");

    
    /**
     * The exception is thrown when biometric authentication fails.
     */
    public static final SecErrorCode BIOMETRIC_AUTHENTICATION_FAILED_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("9100", "Biometric Authentication failed");
    /**
     * The exception is thrown when there is an error in authentication of biometrics.
     */
    public static final SecErrorCode BIOMETRIC_AUTHENTICATION_ERROR_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("9101", "Biometric Authentication error");
    /**
     * The exception is thrown when there is an error in authentication of biometrics.
     */
    public static final SecErrorCode UNKNOWN_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("9102", "Unknown error");
    
    
    
    /** TODO. */
    public static final SecErrorCode NETWORK_POST_JSON_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10001", "JSON parsing exception in network module");
    /**
     * Thrown to indicate that a malformed URL has occurred.
     */
    public static final SecErrorCode MALFORMED_URL_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10002", "Malformed URL exception in network module");
    /**
     * This is the exception for invalid Keys (invalid encoding, wrong length, uninitialized, etc).
     */
    public static final SecErrorCode INVALID_KEY_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10003", "Invalid key exception");
    /**
     * Signals that a method has been invoked at an illegal or inappropriate time.
     */
    public static final SecErrorCode ILLEGAL_STATE_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10004", "Illegal state exception");
    /**
     * This exception is thrown when a particular security provider is requested but is not available in the environment.
     */
    public static final SecErrorCode NO_SUCH_PROVIDER_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10005", "No such provider exception");
    
    /** TODO. */
    public static final SecErrorCode INVALID_ALGO_PROVIDER_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10006", "Invalid algo exception");
    /**
     * This is the generic KeyStore exception.
     */
    public static final SecErrorCode KEYSTORE_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10007", "Keystore exception");
    /**
     * This exception is thrown when a particular cryptographic algorithm is requested but is not available in the environment.
     */
    public static final SecErrorCode NO_SUCH_ALGO_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10008", "No such algo exception");
    /**
     * This exception indicates one of a variety of certificate problems.
     */
    public static final SecErrorCode CERT_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10009", "Certificate exception");
    /**
     * This exception is thrown if a key in the keystore cannot be recovered.
     */
    public static final SecErrorCode UNRECOVERABLE_KEY_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10010", "Unrecoverable key exception");
    /**
     * This exception is thrown if a key in the keystore cannot be recovered.
     */
    public static final SecErrorCode ILLEGAL_BLOCK_SIZE_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10011", "Illegal block size exception");
    /**
     * This exception is thrown when a particular padding mechanism is expected for the input data but the data is not padded properly.
     */
    public static final SecErrorCode BAD_PADDING_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10012", "Bad padding exception");
    /**
     * This exception is thrown when a particular padding mechanism is requested but is not available in the environment.
     */
    public static final SecErrorCode NO_SUCH_PADDING_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10013", "No such padding exception");
    
    /** TODO. */
    public static final SecErrorCode IO_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10014", "IO exception");
    
    /**
     * The exception is thrown when something happens during JSON processing.
     */
    public static final SecErrorCode JSON_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10015", "JSON exception");
    
    /** TODO. */
    public static final SecErrorCode QRCODE_JSON_FOR_TESTING_NOT_FOUND_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10016", "QR Code string not found exception");
    
    /** The exception is thrown when the there is an illegal access. */
    public static final SecErrorCode ILLEGAL_ACCESS_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10017", "Illegal Access Exception");
    
    /** The exception is thrown when the there is an invocation target exception. */
    public static final SecErrorCode INVOCATION_TARGET_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10018", "Invocation target Exception");
    
    /** The exception is thrown when the there is an number format exception. */
    public static final SecErrorCode NUMBER_FORMAT_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10019", "Number format Exception");
    
    /** The exception is thrown when the there is a sql exception. */
    public static final SecErrorCode SQL_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10020", "SQL Exception");
  
    /**
     * The exception is thrown when the user is not authenticated.
     */
    public static final SecErrorCode USER_NOT_AUTHENTICATED_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10021", "User is not authenticated recently enough");
   
    /**
     * The exception is thrown when the Authentication token is invalid.
     */
    public static final SecErrorCode AUTH_TOKEN_INVALID_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10022", "Authentication token is invalid");
    /**
     * The exception is thrown when the Authentication token is missing.
     */
    public static final SecErrorCode AUTH_TOKEN_MISSING_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10023", "Authentication token is missing");
    /**
     * The exception is thrown when there is a error with the client protocol.
     */
    public static final SecErrorCode CLIENT_PROTOCOL_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10024", "Client protocol exception");
    /**
     * The exception is thrown when there is an unsupported operation.
     */
    public static final SecErrorCode UNSUPPORTED_OPERTATION_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10025", "Unsupported operation exception");
    /**
     * The exception is thrown there is an error while creation of JWT.
     */

    public static final SecErrorCode JWT_CREATION_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10026", "Your session has expired or is invalid.");
    
    public static final SecErrorCode JWT_VERIFICATION_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10027", "Your session has expired or is invalid.");

   
    public static final SecErrorCode JWT_EMPTY_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10028", "Your session has expired or is invalid.");

    /**
     * The exception is thrown there is an error while creation of JWT.
     */

   
    public static final SecErrorCode JWT_CLAIM_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10029", "Your session has expired or is invalid.");
    /**
     * The exception is thrown there is an error while creation of JWT.
     */

   
    public static final SecErrorCode JWT_EXPIRED_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10030", "Your session has expired or is invalid.");
   
    /** TODO. */
    public static final SecErrorCode CONNECT_EXCEPTION = SecErrorCodeUtil.getSecErrorCode("10031", "Connect exception");
    
}

