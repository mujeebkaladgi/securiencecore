package com.securience.core.errorhandling;

import java.io.File;
import java.util.Map;

import com.securience.core.Config;
import com.securience.core.SecUtil;
import com.securience.core.logging.SecLogger;

public class SecErrorCodeUtil {

	private static Config.Locale locale = Config.Locale.ENGLISH;
	//private static Config.Locale locale;
	
	public static void setLocale( Config.Locale locale) {
		SecErrorCodeUtil.locale = locale;
	}
	
	private static Map<String, String> PROPS;

	public static Map<String, String> getProps() throws SecException {
		if (PROPS == null) {
			return refreshProps();
		}

		return PROPS;
	}

	public synchronized static Map<String, String> refreshProps() throws SecException {
		
		String filePath = SecUtil.getSecurienceHome("/Users/mohammedmujeeb/Downloads/Securience")
				+ File.separator + "conf" + File.separator + "secuserportal_errorcodes_";
		switch(locale) {
		
			case ENGLISH:
				filePath+="en.properties";
				break;
			case DUTCH:
				filePath+="nl.properties";
				break;
			default:
				filePath+="en.properties";
				break;
		}
			PROPS = SecUtil.readPropertiesFile(filePath);
		return PROPS;
	}
	
	
	
	
	
 	
	public static SecErrorCode getSecErrorCode( 
			String errorCode, String defaultErrorMessage) {
		try {
		if (getProps().get(errorCode) == null) {
			
			return new SecErrorCode(errorCode, defaultErrorMessage);
		}
		

		return new SecErrorCode(errorCode, getProps().get(errorCode));
		} catch(SecException e) {
			SecLogger.error("Error code not found in properties. Code :" + errorCode 
					+ "  DefaultMessage : " + defaultErrorMessage, e);
		}
		return null;
	}
	
	
}
