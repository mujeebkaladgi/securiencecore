package com.securience.core.filter;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
 
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.message.internal.ReaderWriter;

import com.securience.core.logging.SecLogger;
 
// TODO: Auto-generated Javadoc
/**
 * The Class CustomLoggingFilter.
 */
public class CustomLoggingFilter extends LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter 
{
	
	/** The counter. */
	private static int COUNTER = 0;
	
	/** The map. */
	private static Map<Integer,Integer> map = new HashMap<>();
	

	
	
	
    /**
     * Filter.
     *
     * @param requestContext the request context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void filter(ContainerRequestContext requestContext)  throws IOException 
    {
    	int ID = COUNTER++;
    	
    	String prefix = ID + " HTTP REQUEST : ";
    	
    	if(requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SecIAMDemo/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SMGAgent/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SecGateway/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/HGUserPortal/api")) { 
	        StringBuilder sb = new StringBuilder();
	        sb.append("User: ").append(requestContext.getSecurityContext().getUserPrincipal() == null ? "unknown"
	                        : requestContext.getSecurityContext().getUserPrincipal());
	        SecLogger.debug(prefix + sb.toString());
	        sb.setLength(0);
	        sb.append(" - Method: ").append(requestContext.getMethod());
	        SecLogger.debug(prefix  + sb.toString());
	        
	        sb.setLength(0);
	        sb.append(" - Path: ").append(requestContext.getUriInfo().getPath());
	        SecLogger.debug(prefix  + sb.toString());
	        sb.setLength(0);
	        sb.append(" - Header: ").append(requestContext.getHeaders());
	        SecLogger.debug(prefix  + sb.toString());
	        sb.setLength(0);
	        sb.append(" - Entity: ").append(getEntityBody(requestContext));
	        SecLogger.debug(prefix  + sb.toString());
    	}
    }
 
    /**
     * Gets the entity body.
     *
     * @param requestContext the request context
     * @return the entity body
     */
    private String getEntityBody(ContainerRequestContext requestContext) 
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
         
        final StringBuilder b = new StringBuilder();
        try
        {
            ReaderWriter.writeTo(in, out);
 
            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0)
            {
                b.append("").append("\n");
            }
            else
            {
                b.append(new String(requestEntity)).append("\n");
            }
            requestContext.setEntityStream( new ByteArrayInputStream(requestEntity) );
 
        } catch (IOException ex) {
            //Handle logging error
        }
        return b.toString();
    }
 
    /**
     * Filter.
     *
     * @param requestContext the request context
     * @param responseContext the response context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException 
    {
    	int ID = COUNTER++;
    	String prefix = ID + " HTTP RESPONSE : ";
    	if(requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SecIAMDemo/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SMGAgent/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/SecGateway/rest") ||
    			requestContext.getUriInfo().getRequestUri().getRawPath().startsWith("/HGUserPortal/api")
    			) { 
	        StringBuilder sb = new StringBuilder();
	        sb.append("Header: ").append(responseContext.getHeaders());
	        SecLogger.debug(prefix + sb.toString());
	        sb.setLength(0);
	        sb.append(" - Entity: ").append(responseContext.getEntity());
	        SecLogger.debug(prefix + sb.toString());
    	}
    }
}