package com.securience.core.jwt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.securience.core.errorhandling.SecException;

import io.jsonwebtoken.security.SignatureException;

class JwtUtilTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSuccess() {
		
		Map<String, String> jwtPayload = new HashMap<String, String>();
		jwtPayload.put("key1", "value1");
		jwtPayload.put("key2", "value2");
		jwtPayload.put("key3", "value3");
		
		String jwt;
		try {
			jwt = JwtUtil.create(jwtPayload, 10000);
			System.out.println(jwt);
			
			assert(JwtUtil.verify(jwt));
		} catch (SecException e) {
			fail(e.getMessage());
		}	
	}
	
	@Test
	void testFailure() {
		
		Map<String, String> jwtPayload = new HashMap<String, String>();
		jwtPayload.put("key1", "value1");
		jwtPayload.put("key2", "value2");
		jwtPayload.put("key3", "value3");
		
		String jwt;
		try {
			jwt = JwtUtil.create(jwtPayload, 100000);
			System.out.println(jwt);
			
			JwtUtil.verify(jwt+"1");
		} catch (SecException e) {
			
				assert(true);
			
			
		}	
	}

	@Test
	void testPayload() {
		
		Map<String, String> jwtPayload = new HashMap<String, String>();
		jwtPayload.put("key1", "value1");
		jwtPayload.put("key2", "value2");
		jwtPayload.put("key3", "value3");
		
		String jwt;
		try {
			jwt = JwtUtil.create(jwtPayload, 100000);
			System.out.println(jwt);
			
			String value = (String)JwtUtil.getClaimsFromToken(jwt, "key1");
			
			assertEquals("value1", value);
		} catch (SecException e) {
			
				assert(false);
			
			
		}	
	}
}
