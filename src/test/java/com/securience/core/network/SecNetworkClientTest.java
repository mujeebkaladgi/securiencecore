package com.securience.core.network;

import static org.junit.jupiter.api.Assertions.fail;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.securience.core.errorhandling.SecException;

class SecNetworkClientTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGet() throws MalformedURLException, SecException {
		
		SecNetworkClient client = SecNetworkClientFactory.getNetworkClient(SecNetworkConstants.APACHE_HTTP_CLIENT);
		
		SecHttpRequest request = new SecHttpRequest
				.SecHttpRequestBuilder(new URL("https://jsonplaceholder.typicode.com/todos/1"))
				.get()
				.build();
				
		
		SecHttpResponse response = client.invoke(request );
		
		System.out.println(response.getBody());
		assert(response.getBody() != null);
	}

	//@Test
	void testGet_soapUi() throws MalformedURLException, SecException {
		
		SecNetworkClient client = SecNetworkClientFactory.getNetworkClient(SecNetworkConstants.APACHE_HTTP_CLIENT);
		
		SecHttpRequest request = new SecHttpRequest.SecHttpRequestBuilder(new URL("http://localhost:8082/task1"))
				.get()
				.build();
				;
		SecHttpResponse response = client.invoke(request );
		
		System.out.println(response.getBody());
	}
	
	@Test
	void testPost() throws MalformedURLException, SecException {
		
		SecNetworkClient client = SecNetworkClientFactory.getNetworkClient(SecNetworkConstants.APACHE_HTTP_CLIENT);
		
		String body = "{\"name\":\"test\",\"salary\":\"123\",\"age\":\"23\"}";
		
		SecHttpRequest request = new SecHttpRequest.SecHttpRequestBuilder(new URL("http://dummy.restapiexample.com/api/v1/create"))
				.post(body)
				.build();
				;
		SecHttpResponse response = client.invoke(request );
		
		System.out.println(response.getBody());
		assert(response.getBody() != null);
	}
	
	
	
	
}
